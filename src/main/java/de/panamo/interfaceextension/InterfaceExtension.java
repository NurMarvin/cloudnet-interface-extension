package de.panamo.interfaceextension;

import de.dytanic.cloudnet.lib.server.info.ServerInfo;
import de.dytanic.cloudnetcore.api.CoreModule;
import de.dytanic.cloudnetcore.network.components.INetworkComponent;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.dytanic.cloudnetcore.network.components.Wrapper;
import de.panamo.interfaceextension.listener.ScreenInfoListener;
import de.panamo.interfaceextension.listener.ServerListener;
import de.panamo.interfaceextension.socket.InterfaceSocketServer;
import de.panamo.interfaceextension.socket.SocketConsoleSession;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import org.java_websocket.WebSocket;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InterfaceExtension extends CoreModule {
    private static InterfaceExtension instance;

    private InterfaceSocketServer socketServer;
    private Map<WebSocket, SocketConsoleSession> socketConsoleSessions = new HashMap<>();
    private Map<String, List<String>> cachedLogs = new HashMap<>();

    /**
     * Creates and starts the webSocketServer when the module is being enabled
     */

    @Override
    public void onBootstrap() {
        instance = this;

        super.registerListener(new ServerListener.ServerAddListener());
        super.registerListener(new ServerListener.ServerRemoveListener());
        super.registerListener(new ServerListener.ProxyAddListener());
        super.registerListener(new ServerListener.ProxyRemoveListener());
        super.registerListener(new ServerListener.ChannelInitListener());

        super.registerListener(new ScreenInfoListener());

        this.socketServer = new InterfaceSocketServer(this,
                this.loadConfiguration(new File(super.getDataFolder(), "Interface-Extension")));
        this.socketServer.start();
    }

    public void enableScreen(String serverName) {
        MinecraftServer minecraftServer = super.getCloud().getServer(serverName);
        if(minecraftServer != null) {
            minecraftServer.getWrapper().enableScreen(minecraftServer.getServerInfo());
            return;
        }
        ProxyServer proxyServer = super.getCloud().getProxy(serverName);
        if(proxyServer != null)
            proxyServer.getWrapper().enableScreen(proxyServer.getProxyInfo());
    }

    public void disableScreen(String serverName) {
        MinecraftServer minecraftServer = super.getCloud().getServer(serverName);
        if(minecraftServer != null) {
            minecraftServer.getWrapper().disableScreen(minecraftServer.getServerInfo());
            return;
        }
        ProxyServer proxyServer = super.getCloud().getProxy(serverName);
        if(proxyServer != null)
            proxyServer.getWrapper().disableScreen(proxyServer.getProxyInfo());
    }

    private ModuleConfiguration loadConfiguration(File dataFolder) {
        ModuleConfiguration moduleConfiguration = new ModuleConfiguration();

        File configFile = new File(dataFolder, "config.yml");
        if(!configFile.exists()) {
            try {
                if(!dataFolder.exists())
                    dataFolder.mkdir();

                configFile.createNewFile();
                Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

                config.set("tokenSecret", moduleConfiguration.getTokenSecret());
                config.set("webSocketServerPort", moduleConfiguration.getPort());

                ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, configFile);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            return moduleConfiguration;
        }

        try {
            Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            moduleConfiguration.setTokenSecret(config.getString("tokenSecret"));
            moduleConfiguration.setPort(config.getInt("webSocketServerPort"));
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return moduleConfiguration;
    }

    /**
     * Stops the webSocketServer when the module is being disabled
     */

    @Override
    public void onShutdown() {
        try {
            this.socketServer.stop();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void appendLines(String server, List<String> lines) {
        if(!this.cachedLogs.containsKey(server.toLowerCase()))
            this.cachedLogs.put(server.toLowerCase(), new ArrayList<>());

        this.cachedLogs.get(server.toLowerCase()).addAll(lines);
    }

    public Map<String, List<String>> getCachedLogs() {
        return cachedLogs;
    }

    public Map<WebSocket, SocketConsoleSession> getSocketConsoleSessions() {
        return socketConsoleSessions;
    }

    public InterfaceSocketServer getSocketServer() {
        return socketServer;
    }

    public static InterfaceExtension getInstance() {
        return instance;
    }
}
