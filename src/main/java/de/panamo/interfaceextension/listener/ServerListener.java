package de.panamo.interfaceextension.listener;

import de.dytanic.cloudnet.event.IEventListener;
import de.dytanic.cloudnetcore.api.event.network.ChannelInitEvent;
import de.dytanic.cloudnetcore.api.event.server.ProxyAddEvent;
import de.dytanic.cloudnetcore.api.event.server.ProxyRemoveEvent;
import de.dytanic.cloudnetcore.api.event.server.ServerAddEvent;
import de.dytanic.cloudnetcore.api.event.server.ServerRemoveEvent;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.panamo.interfaceextension.InterfaceExtension;
import de.panamo.interfaceextension.socket.InterfaceSocketServer;

public class ServerListener {
    private static InterfaceSocketServer interfaceSocketServer = InterfaceExtension.getInstance().getSocketServer();

    public static class ServerAddListener implements IEventListener<ServerAddEvent> {

        @Override
        public void onCall(ServerAddEvent event) {
            interfaceSocketServer.broadcastNotification("Der Server " + event.getMinecraftServer().getName() + " wird gestartet ...");
        }
    }

    public static class ProxyAddListener implements IEventListener<ProxyAddEvent> {

        @Override
        public void onCall(ProxyAddEvent event) {
            interfaceSocketServer.broadcastNotification("Die Proxy " + event.getProxyServer().getName() + " wird gestartet ...");
        }
    }

    public static class ServerRemoveListener implements IEventListener<ServerRemoveEvent> {

        @Override
        public void onCall(ServerRemoveEvent event) {
            interfaceSocketServer.broadcastNotification("Der Server " + event.getMinecraftServer().getName() + " wird gestoppt ...");
        }
    }

    public static class ProxyRemoveListener implements IEventListener<ProxyRemoveEvent> {

        @Override
        public void onCall(ProxyRemoveEvent event) {
            interfaceSocketServer.broadcastNotification("Die Proxy " + event.getProxyServer().getName() + " wird gestoppt ...");
        }
    }

    public static class ChannelInitListener implements IEventListener<ChannelInitEvent> {

        @Override
        public void onCall(ChannelInitEvent event) {
            String name = event.getINetworkComponent().getServerId();
            if(event.getINetworkComponent() instanceof MinecraftServer)
                interfaceSocketServer.broadcastNotification("Der Server " + name + " wurde gestartet");
            else if(event.getINetworkComponent() instanceof ProxyServer)
                interfaceSocketServer.broadcastNotification("Die Proxy " + name + " wurde gestartet");
        }
    }
}
