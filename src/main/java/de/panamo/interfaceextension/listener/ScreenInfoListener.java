package de.panamo.interfaceextension.listener;

import de.dytanic.cloudnet.event.IEventListener;
import de.dytanic.cloudnet.lib.server.screen.ScreenInfo;
import de.dytanic.cloudnetcore.api.event.server.ScreenInfoEvent;
import de.panamo.interfaceextension.InterfaceExtension;
import de.panamo.interfaceextension.socket.SocketConsoleSession;

public class ScreenInfoListener implements IEventListener<ScreenInfoEvent> {

    @Override
    public void onCall(ScreenInfoEvent event) {
        for(ScreenInfo screenInfo : event.getScreenInfos()) {
            for (SocketConsoleSession socketConsoleSession : InterfaceExtension.getInstance().getSocketConsoleSessions().values()) {
                System.out.println(screenInfo.getLine());
                if(socketConsoleSession.getServer().equalsIgnoreCase(screenInfo.getServiceId().getServerId()))
                    socketConsoleSession.sendLogLines(screenInfo.getLine());
            }
        }
    }
}
