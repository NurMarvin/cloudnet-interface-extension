package de.panamo.interfaceextension.socket;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.JsonSyntaxException;
import de.dytanic.cloudnet.lib.NetworkUtils;
import de.dytanic.cloudnet.lib.map.WrappedMap;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.dytanic.cloudnet.lib.server.*;
import de.dytanic.cloudnet.lib.server.advanced.AdvancedServerConfig;
import de.dytanic.cloudnet.lib.server.template.Template;
import de.dytanic.cloudnet.lib.server.template.TemplateResource;
import de.dytanic.cloudnet.lib.server.version.ProxyVersion;
import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnet.lib.utility.threading.Scheduler;
import de.dytanic.cloudnetcore.CloudNet;
import de.dytanic.cloudnetcore.api.CoreModule;
import de.dytanic.cloudnetcore.database.StatisticManager;
import de.dytanic.cloudnetcore.network.components.*;
import de.dytanic.cloudnetcore.util.defaults.BasicProxyConfig;
import de.panamo.interfaceextension.InterfaceExtension;
import de.panamo.interfaceextension.ModuleConfiguration;
import org.java_websocket.WebSocket;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class InterfaceSocketServer extends WebSocketServer {
    private static final String PREFIX = "[WebSocketServer] ";
    private CoreModule coreModule;
    private Map<WebSocket, String> authorizedSockets = new HashMap<>();
    private Algorithm tokenAlgorithm;
    private JWTCreator.Builder jwtBuilder;
    private JWTVerifier jwtVerifier;

    public InterfaceSocketServer(CoreModule coreModule, ModuleConfiguration moduleConfiguration) {
        super(new InetSocketAddress(coreModule.getCloud().getConfig().getWebServerConfig().getAddress(),
                moduleConfiguration.getPort()));
        this.coreModule = coreModule;
        this.tokenAlgorithm = Algorithm.HMAC256(moduleConfiguration.getTokenSecret());
        this.jwtBuilder = JWT.create().withIssuer("cloudNetInterface");
        this.jwtVerifier = JWT.require(this.tokenAlgorithm).withIssuer("cloudNetInterface").build();
    }

    /**
     * Handles the start of the server
     */

    @Override
    public void onStart() {
        System.out.println(PREFIX + "Successfully bound WebSocketServer to " + super.getAddress().toString());
        Scheduler scheduler = this.coreModule.getCloud().getScheduler();
        scheduler.runTaskRepeatSync(() -> {
            for(WebSocket webSocket : super.getConnections()) {
                if(!this.authorizedSockets.containsKey(webSocket)) {
                    System.out.println(PREFIX + "Closing WebSocket " + webSocket.getRemoteSocketAddress().toString()
                            + " because of missing authentication");
                    webSocket.close(CloseFrame.NORMAL, "Permission denied");
                }
            }
        }, scheduler.getTicks(), scheduler.getTicks() * 10);

    }

    /**
     * Handles a new socket-connection
     *
     * @param webSocket the socket
     * @param clientHandshake the handshake
     */

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("New WebSocket from " + webSocket.getRemoteSocketAddress().toString());
    }

    /**
     * Handles messages from a socket
     *
     * @param webSocket the socket
     * @param jsonMessage the message
     */

    @Override
    public void onMessage(WebSocket webSocket, String jsonMessage) {
        Document document = Document.load(jsonMessage);
        if(document.getString("message") != null) {
            String message = document.getString("message").toLowerCase();
            String[] values = document.getObject("values", String[].class);

            Document responseDocument = new Document().append("message", document.getString("message")).append("response", new Document());

            // Handle auth
            if(message.equals("auth") && values.length > 1) {
                String user = values[0];
                String password = values[1];
                if (this.coreModule.getCloud().authorizationPassword(user, password)) {
                    // Add socket to the authorized sockets
                    System.out.println(PREFIX + "WebSocket " + webSocket.getRemoteSocketAddress().toString() + " authorized successfully "
                            + "with user " + user);
                    this.authorizedSockets.put(webSocket, user);
                    responseDocument.append("response", Arrays.asList(this.createUserToken(user), this.coreModule.getVersion()));
                } else {
                    webSocket.close(CloseFrame.NORMAL, "Permission denied");
                    return;
                }
            } else if(message.equals("auth-token") && values.length == 1) {
                String user = this.verifyToken(values[0]);
                if (user != null) {
                    // Add socket to the authorized sockets
                    System.out.println(PREFIX + "WebSocket " + webSocket.getRemoteSocketAddress().toString() + " authorized successfully "
                            + "with user " + user);
                    this.authorizedSockets.put(webSocket, user);
                    responseDocument.append("response", this.coreModule.getVersion());
                } else {
                    webSocket.close(CloseFrame.NORMAL, "Permission denied");
                    return;
                }
            } else if(!this.authorizedSockets.containsKey(webSocket)) {
                webSocket.close(CloseFrame.NORMAL, "Permission denied");
                return;
            } else
                this.handleSuccessfulMessage(message, values, responseDocument, webSocket,
                        this.coreModule.getCloud().getUser(this.authorizedSockets.get(webSocket)));
            webSocket.send(responseDocument.convertToJsonString());
        }
    }

    /**
     * Sends a notification that will show in all connected interfaces
     *
     * @param message the notification-message
     */

    public void broadcastNotification(String message) {
        Document responseDocument = new Document("message", "notification").append("content", message);
        super.broadcast(responseDocument.convertToJsonString());
    }

    /**
     * Creates an authentication token for an user, which expires in 3 days
     *
     * @param user the user
     * @return the token
     */

    private String createUserToken(String user) {
        return this.jwtBuilder.withClaim("user", user).withExpiresAt(
                new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(3))).sign(this.tokenAlgorithm);
    }

    /**
     * Verifies a token
     *
     * @param token the token
     * @return the user of the token or null, if the token is invalid
     */

    private String verifyToken(String token) {
        try {
            DecodedJWT decodedJWT = this.jwtVerifier.verify(token);
            Claim claim = decodedJWT.getClaim("user");
            return claim.asString();
        } catch (JWTVerificationException exception) {
            System.out.println("Invalid token");
            return null;
        }

    }

    /**
     * Handles an error which happened because of a certain socket
     *
     * @param webSocket the socket
     * @param exception the error
     */

    @Override
    public void onError(WebSocket webSocket, Exception exception) {
        if(exception == null)
            return;
        if(webSocket == null)
            System.err.println(PREFIX + "Error at the WebSocketServer: " + exception.getMessage());
        else {
            System.err.println(PREFIX + "Error with WebSocket " + webSocket.getRemoteSocketAddress().toString() +
                    ": " + exception.getMessage());
            if(exception instanceof JsonSyntaxException)
                webSocket.close(CloseFrame.REFUSE, "Wrong JSON");
        }
        exception.printStackTrace();

    }

    /**
     * Handles a disconnection of a certain socket
     *
     * @param webSocket the socket
     * @param code the closeCode
     * @param reason the reason
     * @param remote remote
     */

    @Override
    public void onClose(WebSocket webSocket, int code, String reason, boolean remote) {
        System.out.println(PREFIX + "WebSocket from " +  webSocket.getRemoteSocketAddress().toString() +
                " disconnected with reason: " + reason + " (Code " + code + ")");
    }

    /**
     * Checks if an user has a certain permission
     *
     * @param user the user
     * @param permission the permission
     * @return if the user has permission
     */

    private boolean checkUserPermission(User user, String permission) {
        PermissionCategory permissionCategory = PermissionCategory.getFromPermission(permission);
        if(permissionCategory == null)
            return false;
        if(user.hasPermission(permissionCategory.getCategoryPermission()) || user.hasPermission("*"))
            return !this.getNegativePermissions(user).contains(permission);
        return user.hasPermission(permission);
    }

    /**
     * Returns negative permissions of a certain user
     *
     * @param user the user
     * @return the negative permissions as normal permissions
     */

    private Collection<String> getNegativePermissions(User user) {
        ArrayList<String> negativePermissions = new ArrayList<>();
        for(String permission : user.getPermissions()) {
            if (permission.startsWith("-"))
                negativePermissions.add(permission.substring(1, permission.length() -1).toLowerCase());
        }
        return negativePermissions;
    }

    /**
     * Handles a message from an authenticated webSocket
     *
     * @param message the message
     * @param values values of the message
     * @param responseDocument the document which is been sent as the response
     * @param user the cloudNet-user of the webSocket
     */

    private void handleSuccessfulMessage(String message, String[] values, Document responseDocument, WebSocket webSocket, User user) {
        if(!checkUserPermission(user, "cloudnet.web." + message)) {
            responseDocument.append("response", "Permission denied");
            return;
        }
        switch (message) {
            case "serverinfos":
                Document response = new Document();
                for (MinecraftServer minecraftServer : this.coreModule.getCloud().getServers().values())
                    response.append(minecraftServer.getServiceId().getServerId(), minecraftServer.getServerInfo());

                responseDocument.append("response", response);
                break;
            case "proxyinfos":
                Document proxyResponse = new Document();
                for (ProxyServer minecraftServer : this.coreModule.getCloud().getProxys().values())
                    proxyResponse.append(minecraftServer.getServiceId().getServerId(), minecraftServer.getProxyInfo());

                responseDocument.append("response", proxyResponse);
                break;
            case "userinfo":
                Document userResponse = new Document();
                userResponse.append("name", user.getName());
                userResponse.append("uuid", user.getUniqueId().toString());
                userResponse.append("permissions", user.getPermissions());

                responseDocument.append("response", userResponse);
                break;
            case "onlineplayers":
                Document playerResponse = new Document();
                for (CloudPlayer cloudPlayer : this.coreModule.getCloud().getNetworkManager().getOnlinePlayers().values())
                    playerResponse.append(cloudPlayer.getUniqueId().toString(), cloudPlayer);

                responseDocument.append("response", playerResponse);
                break;
            case "statistics":
                responseDocument.append("response", StatisticManager.getInstance().getStatistics());
                break;
            case "cloudnetwork":
                responseDocument.append("response", this.coreModule.getCloud().getNetworkManager().newCloudNetwork());
                break;
            case "memory":
                Document memoryDocument = new Document()
                        .append("maxMemory", this.coreModule.getCloud().globalMaxMemory())
                        .append("usedMemory", this.coreModule.getCloud().globalUsedMemory());
                responseDocument.append("response", memoryDocument);
                break;
            case "cpuusage":
                double cpuUsage = 0.0D;
                for (Wrapper wrapper : this.coreModule.getCloud().getWrappers().values())
                    cpuUsage += wrapper.getCpuUsage();
                cpuUsage /= this.coreModule.getCloud().getWrappers().size();
                responseDocument.append("response", cpuUsage);
                break;
            case "log":
                INetworkComponent server = this.coreModule.getCloud().getServer(values[0])
                        != null ? this.coreModule.getCloud().getServer(values[0]) : this.coreModule.getCloud().getProxy(values[0]);
                if (server != null) {
                    String randomString = NetworkUtils.randomString(10);
                    this.coreModule.getCloud().getServerLogManager().append(randomString, server.getServerId());
                    String logLink = new StringBuilder(this.coreModule.getCloud().getOptionSet().has("ssl") ? "https://" : "http://").append(this.coreModule.getCloud().getConfig().getWebServerConfig()
                            .getAddress()).append(":").append(this.coreModule.getCloud().getConfig().getWebServerConfig().getPort()).append("/cloudnet/log?server=").append(randomString).substring(0);
                    responseDocument.append("response", logLink);
                } else
                    responseDocument.append("response", "No server found");

                break;
            case "startconsolesession":
                InterfaceExtension.getInstance().enableScreen(values[0]);
                SocketConsoleSession socketConsoleSession = new SocketConsoleSession(webSocket, values[0]);
                InterfaceExtension.getInstance().getSocketConsoleSessions().put(webSocket, socketConsoleSession);
                socketConsoleSession.sendExistingLines();
                break;
            case "stopconsolesession":
                if(InterfaceExtension.getInstance().getSocketConsoleSessions().containsKey(webSocket)) {
                    SocketConsoleSession session = InterfaceExtension.getInstance().getSocketConsoleSessions().get(webSocket);
                    InterfaceExtension.getInstance().disableScreen(session.getServer());
                    InterfaceExtension.getInstance().getSocketConsoleSessions().remove(webSocket);
                }
                break;
            case "startserver":
                this.coreModule.getCloud().getScheduler().runTaskSync(() -> this.coreModule.getCloud().startGameServer(this.coreModule.getCloud().getServerGroup(values[0])));
                break;
            case "startproxy":
                this.coreModule.getCloud().getScheduler().runTaskSync(() -> this.coreModule.getCloud().startProxy(this.coreModule.getCloud().getProxyGroup(values[0])));
                break;
            case "stopserver":
                this.coreModule.getCloud().getScheduler().runTaskSync(() -> this.coreModule.getCloud().stopServer(values[0]));
                break;
            case "stopproxy":
                this.coreModule.getCloud().getScheduler().runTaskSync(() -> this.coreModule.getCloud().stopProxy(values[0]));
                break;
            case "createservergroup":
                ServerGroup serverGroup = new ServerGroup(
                        values[0],
                        Collections.singletonList(values[1]),
                        true,
                        Integer.valueOf(values[2]),
                        Integer.valueOf(values[2]),
                        0,
                        false,
                        Integer.valueOf(values[3]),
                        Integer.valueOf(values[4]),
                        Integer.valueOf(values[5]),
                        180,
                        100,
                        100,
                        Integer.valueOf(values[6]),
                        ServerGroupType.valueOf(values[7]),
                        ServerGroupMode.valueOf(values[8]),
                        Arrays.asList(new Template(
                                "default",
                                TemplateResource.valueOf(values[9]),
                                null,
                                new String[0],
                                new ArrayList<>()
                        )),
                        new AdvancedServerConfig(false,
                                false, false, !ServerGroupMode.valueOf(values[8]).equals(ServerGroupMode.STATIC)));
                this.coreModule.getCloud().getConfig().createGroup(serverGroup);
                break;
            case "createproxygroup":
                ProxyGroup proxyGroup = new ProxyGroup(values[0], Collections.singletonList(values[1]), new Template(
                        "default",
                        TemplateResource.valueOf(values[2]),
                        null,
                        new String[0],
                        new ArrayList<>()
                ), ProxyVersion.BUNGEECORD, Integer.valueOf(values[3]), Integer.valueOf(values[4]), Integer.valueOf(values[5]), new BasicProxyConfig(),
                        ProxyGroupMode.valueOf(values[6]), new WrappedMap());
                this.coreModule.getCloud().getConfig().createGroup(proxyGroup);
                break;
            case "createwrapper":
                WrapperMeta wrapperMeta = new WrapperMeta(values[0], values[1], values[2]);
                this.coreModule.getCloud().getConfig().createWrapper(wrapperMeta);
                break;
            case "deleteservergroup":
                if (this.coreModule.getCloud().getServerGroups().containsKey(values[0])) {
                    this.coreModule.getCloud().getConfig().deleteGroup(this.coreModule.getCloud().getServerGroups().get(values[0]));
                    this.coreModule.getCloud().getServerGroups().remove(values[0]);
                    this.coreModule.getCloud().getNetworkManager().updateAll();
                    for (MinecraftServer minecraftServer : this.coreModule.getCloud().getServers(values[0])) 
                        minecraftServer.getWrapper().stopServer(minecraftServer);
                } else
                    responseDocument.append("response", "No group found");

                break;
            case "deleteproxygroup":
                if (this.coreModule.getCloud().getProxyGroups().containsKey(values[0])) {
                    this.coreModule.getCloud().getConfig().deleteGroup(this.coreModule.getCloud().getProxyGroups().get(values[0]));
                    this.coreModule.getCloud().getProxyGroups().remove(values[0]);
                    this.coreModule.getCloud().getNetworkManager().updateAll();
                    for (ProxyServer minecraftServer : this.coreModule.getCloud().getProxys(values[0]))
                        minecraftServer.getWrapper().stopProxy(minecraftServer);
                } else
                    responseDocument.append("response", "No group found");
                break;
        }
    }

    public enum PermissionCategory {
        INFO_PERMISSIONS("cloudnet.web.info", "cloudnet.web.serverinfos", "cloudnet.web.proxyinfos", "cloudnet.web.userinfo",
                "cloudnet.web.onlineplayers", "cloudnet.web.statistics", "cloudnet.web.cloudnetwork", "cloudnet.web.memory",
                "cloudnet.web.cpuusage", "cloudnet.web.log", "cloudnet.web.startconsolesession", "cloudnet.web.stopconsolesession"),
        START_STOP_PERMISSIONS("cloudnet.web.start.stop", "cloudnet.web.startserver", "cloudnet.web.startproxy",
                "cloudnet.web.stopserver", "cloudnet.web.stopproxy"),
        CREATE_DELETE_PERMISSIONS("cloudnet.web.create.delete", "cloudnet.web.createservergroup", "cloudnet.web.createproxygroup",
                "cloudnet.web.createwrapper", "cloudnet.web.deleteservergroup", "cloudnet.web.deleteproxygroup");

        private String categoryPermission;
        private Collection<String> permissions;

        PermissionCategory(String categoryPermission, String... permissions) {
            this.categoryPermission = categoryPermission;
            this.permissions = Arrays.asList(permissions);
        }

        public String getCategoryPermission() {
            return categoryPermission;
        }

        public Collection<String> getPermissions() {
            return permissions;
        }

        public static PermissionCategory getFromPermission(String permission) {
            for(PermissionCategory permissionCategory : values()) {
                if(permissionCategory.getPermissions().contains(permission.toLowerCase()))
                    return permissionCategory;
            }
            return null;
        }
    }

}
