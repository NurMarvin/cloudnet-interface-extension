package de.panamo.interfaceextension.socket;

import de.dytanic.cloudnet.lib.utility.document.Document;
import de.panamo.interfaceextension.InterfaceExtension;
import org.java_websocket.WebSocket;
import java.util.Arrays;
import java.util.List;

public class SocketConsoleSession {
    private WebSocket webSocket;
    private String server;

    SocketConsoleSession(WebSocket webSocket, String server) {
        this.webSocket = webSocket;
        this.server = server;
    }

    public void sendExistingLines() {
        if(InterfaceExtension.getInstance().getCachedLogs().containsKey(this.server.toLowerCase()))
            this.sendLogLines0(InterfaceExtension.getInstance().getCachedLogs().get(this.server.toLowerCase()));
    }

    public void sendLogLines(String... lines) {
        List<String> lineList = Arrays.asList(lines);
        InterfaceExtension.getInstance().appendLines(this.server, lineList);
        this.sendLogLines0(lineList);
    }

    private void sendLogLines0(List<String> lines) {
        Document document = new Document("message", "consoleLine").append("content", lines);
        this.webSocket.send(document.convertToJsonString());
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }

    public String getServer() {
        return server;
    }
}
